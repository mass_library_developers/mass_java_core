package edu.uw.bothell.css.dsl.MASS.cypher.executionPlan;

import edu.uw.bothell.css.dsl.MASS.cypher.SingleRowPropertyGraphCypherResult;
import edu.uw.bothell.css.dsl.MASS.cypher.PropertyGraphCypherQueryContext;
import edu.uw.bothell.css.dsl.MASS.cypher.PropertyGraphCypherResult;
import edu.uw.bothell.css.dsl.MASS.cypher.exceptions.PropertyGraphCypherNotImplemented;
import edu.uw.bothell.css.dsl.MASS.cypher.utils.StringUtils;

import java.util.List;

public class CreatePatternExecutionStep extends ExecutionStepWithChildren {
    private final String name;

    public CreatePatternExecutionStep(
        String name,
        List<CreateNodePatternExecutionStep> createNodeExecutionSteps,
        List<CreateRelationshipPatternExecutionStep> createRelationshipExecutionSteps
    ) {
        super(toChildren(createNodeExecutionSteps, createRelationshipExecutionSteps));
        this.name = name;
    }

    private static ExecutionStep[] toChildren(
        List<CreateNodePatternExecutionStep> createNodeExecutionSteps,
        List<CreateRelationshipPatternExecutionStep> createRelationshipExecutionSteps
    ) {
        ExecutionStep[] results = new ExecutionStep[createNodeExecutionSteps.size() + createRelationshipExecutionSteps.size()];
        int i = 0;
        for (CreateNodePatternExecutionStep createNodeExecutionStep : createNodeExecutionSteps) {
            results[i++] = createNodeExecutionStep;
        }
        for (CreateRelationshipPatternExecutionStep createRelationshipExecutionStep : createRelationshipExecutionSteps) {
            results[i++] = createRelationshipExecutionStep;
        }
        return results;
    }

    @Override
    public PropertyGraphCypherResult execute(PropertyGraphCypherQueryContext ctx, PropertyGraphCypherResult source) {
        if (source == null) {
            source = new SingleRowPropertyGraphCypherResult();
        }

        source = super.execute(ctx, source);

        if (name != null) {
            throw new PropertyGraphCypherNotImplemented("name");
        }

        return source;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("In %s:", super.toString()));
        getChildSteps().forEach(child -> {
            String childString = StringUtils.indent(2, child.toString());
            result.append('\n').append(childString);
        });
        return result.toString();
    }
}
