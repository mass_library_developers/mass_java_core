package edu.uw.bothell.css.dsl.MASS.cypher.exceptions;

public class PropertyGraphCypherConstraintVerificationFailedException extends PropertyGraphCypherException {
    private static final long serialVersionUID = 1070814441992090904L;

    public PropertyGraphCypherConstraintVerificationFailedException(String message) {
        super(message);
    }
}
