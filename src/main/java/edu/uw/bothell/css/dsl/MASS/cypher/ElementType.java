package edu.uw.bothell.css.dsl.MASS.cypher;

public enum ElementType {
    NODE,
    EDGE;
}
