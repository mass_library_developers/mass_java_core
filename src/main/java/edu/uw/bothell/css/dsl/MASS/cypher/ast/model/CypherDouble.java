package edu.uw.bothell.css.dsl.MASS.cypher.ast.model;

public class CypherDouble extends CypherLiteral<Double> {
    public CypherDouble(Double value) {
        super(value);
    }
}
