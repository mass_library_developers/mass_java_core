package edu.uw.bothell.css.dsl.MASS.cypher.ast.model;

public class CypherBoolean extends CypherLiteral<Boolean> {
    public CypherBoolean(Boolean value) {
        super(value);
    }
}
