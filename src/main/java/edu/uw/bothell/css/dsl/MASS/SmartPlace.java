/*

 	MASS Java Software License
	© 2012-2021 University of Washington

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	The following acknowledgment shall be used where appropriate in publications, presentations, etc.:

	© 2012-2021 University of Washington. MASS was developed by Computing and Software Systems at University of
	Washington Bothell.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

*/

package edu.uw.bothell.css.dsl.MASS;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.*;

import edu.uw.bothell.css.dsl.MASS.matrix.MatrixUtilities;

public class SmartPlace extends Place{

    public static final int COLLECT_AGENTS = 2;

    private int handle;
    private double[] min;  //min value of the SpacePlace (not whole places)
    private double[] max;

    private double[] subInterval;  // interval of sub-place
    private int granularity;

    //Generic Attributes that hold Neighbors and Distances
    public int[] neighbors = null;
    public int[] distances = null;
    public int footprint = -1;


    // hashtable to keep all agents in the place, key is agent's linearSubIndex, value is a list of agents in that subPlace
    private Hashtable<Integer, Set<Agent>> agentsMap = new Hashtable<>();

    public SmartPlace( ) {
        super( );
    }

    public SmartPlace(Object args) {
        super();
    }

    public Object initNetwork( ) {
        // My network information
        int nNodes = getSize( )[0];
        int nodeId = getIndex( )[0];

        MASS.getLogger( ).debug( "Initializing Network with number of nodes as : " + nNodes + " and Initial NodeId as : " +nodeId);

        // Generate my neighbors and their distances
        Map( nNodes, nodeId );

        return null;
    }

    private Object Map( int nNodes, int nodeId ) {

        // Prepare temporal storages for neighboring information
        ArrayList<Integer> neighborsList = new ArrayList<Integer>( );
        ArrayList<Integer> distancesList = new ArrayList<Integer>( );

        // All nodes have the same random number generator
        Random rand = new Random( 0 );
        // All nodes have the same upper limit
        int upperLimit = ( int )( nNodes * 0.3 );

        for ( int i = 0; i < nNodes; i++ ) {
            // Generate # neighbors from node i
            int nNeighbors = rand.nextInt( upperLimit );
            if ( nNeighbors == 0 ) nNeighbors = 1;

            // Generate neighboring nodes from node i
            for ( int j = 0; j < nNeighbors; j++ ) {
                int neighbor = rand.nextInt( nNodes );
                int distance = rand.nextInt( nNodes );
                if ( distance == 0 ) distance = 1;
                if ( i == nodeId ) {
                    // check if this is a self-directed link
                    if ( neighbor == nodeId )
                        continue;
                    // check if this is a duplication
                    if ( neighborsList.indexOf( new Integer( neighbor ) )
                            == -1 ) {
                        // store my neighbor and its distance
                        neighborsList.add( new Integer( neighbor ) );
                        distancesList.add( new Integer( distance ) );
                    }
                }
                else if ( neighbor == nodeId ) {
                    // check if this is a duplication
                    if ( neighborsList.indexOf( new Integer( i ) )
                            == -1 ) {
                        // store my neighbor and its distance
                        neighborsList.add( new Integer( i ) );
                        distancesList.add( new Integer( distance ) );
                    }
                }
            }
        }

        neighbors = new int[neighborsList.size( )];
        distances = new int[distancesList.size( )];

        MASS.getLogger( ).debug( "Number of neighbors created for the node : " + nodeId + " is : " +neighborsList.size( ));

        for ( int i = 0; i < neighborsList.size( ); i++ ) {
            neighbors[i] = neighborsList.get( i );
            distances[i] = distancesList.get( i );
        }

        return null;
    }

}
