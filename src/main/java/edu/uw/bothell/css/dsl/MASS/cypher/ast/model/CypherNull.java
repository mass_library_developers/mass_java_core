package edu.uw.bothell.css.dsl.MASS.cypher.ast.model;

public class CypherNull extends CypherLiteral<Object> {
    public CypherNull() {
        super(null);
    }
}
