package edu.uw.bothell.css.dsl.MASS.cypher.exceptions;

public class PropertyGraphCypherArgumentErrorException extends PropertyGraphCypherException {
    private static final long serialVersionUID = 1615499957071320659L;

    public PropertyGraphCypherArgumentErrorException(String message) {
        super(message);
    }
}
